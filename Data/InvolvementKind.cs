﻿using System;
namespace Cognitor.Data {
    public enum InvolvementKind {
        Unknown,
        Client,
        Opponent,
        Representative,
        Court,
        Agency
    }
}
