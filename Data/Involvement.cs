﻿using System;
using System.Collections.Generic;

namespace Cognitor.Data {
    public sealed class Involvement {

        public int Id {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the involved party.
        /// </summary>
        /// <value>The party.</value>
        public Party Party {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the kind of involvement represented.
        /// </summary>
        /// <value>The kind.</value>
        public InvolvementKind Kind {
            get;
            set;
        }

        /// <summary>
        /// Gets the potential child involvements.
        /// </summary>
        /// <value>The children.</value>
        public ICollection<Involvement> Children {
            get {
                return _children;
            }
        }

        List<Involvement> _children = new List<Involvement> ();
    }
}
