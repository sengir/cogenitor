﻿using System;
namespace Cognitor.Data {
    public sealed class Document {

        public int Id {
            get;
            set;
        }

        public Party Recipient {
            get;
            set;
        }

        public Party Sender {
            get;
            set;
        }

        public long FileDate {
            get;
            set;
        }

    }
}
