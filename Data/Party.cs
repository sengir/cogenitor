﻿using System;
namespace Cognitor.Data {

    public sealed class Party {

        public int Id {
            get;
            set;
        }

        public string FirstName {
            get;
            set;
        }

        public string LastName {
            get;
            set;
        }
    }
}
