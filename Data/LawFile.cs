﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Cognitor.Data {

    public sealed class LawFile {

        public int Id {
            get;
            set;
        }

        /// <summary>
        /// Gets the state of the file.
        /// </summary>
        /// <value>The state.</value>
        public FileState State {
            get;
            private set;
        }

        /// <summary>
        /// Gets the creation date in ticks.
        /// </summary>
        /// <value>The creation date.</value>
        public long CreationDate {
            get;
            private set;
        }

        /// <summary>
        /// Gets the date of archivation for this file.
        /// </summary>
        /// <value>The archivation date.</value>
        public long ArchivationDate {
            get;
            private set;
        }

        /// <summary>
        /// Gets a collection of all (primary) parties involved in the file.
        /// </summary>
        /// <value>The involvements.</value>
        public ICollection<Involvement> Involvements {
            get {
                return _involvements;
            }
        }

        /// <summary>
        /// Gets a collection of documents associated with this file.
        /// </summary>
        /// <value>The documents.</value>
        public ICollection<Document> Documents {
            get {
                return _documents;
            }
        }

        List<Involvement> _involvements = new List<Involvement> ();
        List<Document> _documents = new List<Document> ();

        public LawFile () {
            CreationDate = DateTime.Now.Ticks;
            State = FileState.Active;
        }

        public void Archive () {
            ArchivationDate = DateTime.Now.Ticks;
            State = FileState.Archived;
        }
    }
}
