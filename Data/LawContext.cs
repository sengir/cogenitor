﻿using System;
using System.Data.Common;
using System.Data.Entity;

namespace Cognitor.Data {
    public class LawContext : DbContext {

        public LawContext (DbConnection connection) : base (connection, false) {
        }

        public LawContext () : base ("name=MyContext") {
        }

        public DbSet<LawFile> Files { get; set; }
        public DbSet<Party> Parties { get; set; }
    }
}
