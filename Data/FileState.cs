﻿using System;
namespace Cognitor.Data {
    public enum FileState {
        Unknown,
        /// <summary>
        /// Active file.
        /// </summary>
        Active,
        /// <summary>
        /// Archived file
        /// </summary>
        Archived,
        /// <summary>
        /// Lost file
        /// </summary>
        Lost
    }
}
