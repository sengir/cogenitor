﻿using System;
using Cognitor.Utils;

namespace Cognitor {
    public static class Globals {

        public static string InstancePath = PATH_INSTANCE_DEFAULT;

        public static FolderManager Folders;

        public const string PATH_APPDATA = "Cogenitor/";
        public const string PATH_INSTANCE_DEFAULT = "Current/";
        public const string PATH_PLUGINS = "Plugins/";
        public const string PATH_LOG = "Log/";
        public const string PATH_SETTINGS = "Settings/";
        public const string PATH_CACHE = "Cache/";

    }
}
