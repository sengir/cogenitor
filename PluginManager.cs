﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Cognitor.Json;
using Cognitor.Utils;

namespace Cognitor {
    public static class PluginManager {

        public static void Init (IStatusReporter reporter) {

            reporter.SetStatus ("Suche Plugins...");

            List<IPlugin> plugins = new List<IPlugin> ();
            FileInfo [] candidates = GetPluginAssemblies ();

            reporter.SetStatus (string.Format ("{0} Kandidaten verfügbar...", candidates.Count ()));
            foreach (FileInfo candidate in candidates) {
                try {
                    Assembly assembly = ReflectionUtils.LoadAssemblyFromFile (candidate.FullName);
                    if (assembly == null) {
                        reporter.SetStatus (string.Format ("Ignoriere {0}...", candidate));
                        continue;
                    }

                    string clazz = string.Format ("{0}.Plugin", assembly.GetName ().Name);

                    Type type = assembly.GetType (clazz, true);
                    IPlugin plugin = (IPlugin)Activator.CreateInstance (type);
                    plugins.Add (plugin);
                    reporter.SetStatus (string.Format ("Geladen: {0}", clazz));

                } catch (Exception ex) {
                    reporter.SetStatus (string.Format ("Ignoriere fehlerhaftes Plugin {0}: ", candidate, ex.Message));
                }
            }

        }

        static FileInfo [] GetPluginAssemblies () {
            DirectoryInfo plugindir = new DirectoryInfo (Path.Combine (new FileInfo (Assembly.GetExecutingAssembly ().Location).Directory.FullName, Globals.PATH_PLUGINS)); ;
            if (!plugindir.Exists) {
                return new FileInfo [0];
            }

            return plugindir.GetFiles ("*?.dll").OrderBy (f => f.FullName).ToArray ();
        }

        static Stream OpenExact (Assembly assembly, string pattern) {
            string url = assembly.GetManifestResourceNames ().OrderByDescending (p => p).Where (p => p.EndsWith (pattern)).FirstOrDefault ();
            if (string.IsNullOrEmpty (url)) {
                return null;
            }
            return assembly.GetManifestResourceStream (url);
        }

    }
}
