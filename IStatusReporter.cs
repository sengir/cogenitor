﻿using System;
namespace Cognitor {
    public interface IStatusReporter {
        void SetStatus (string status);
    }
}
