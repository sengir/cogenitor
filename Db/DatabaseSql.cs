﻿using System;
using System.Data.Common;

namespace Cognitor.Db {
    public class DatabaseSql : IDatabase {

        DbProviderFactory _factory;
        DbConnection _connection;

        string _server;
        int _port;
        string _database;
        string _uid;
        string _password;

        public DatabaseSql (string server, string database, string uid, string password)
            : this (server, 3306, database, uid, password) {
        }

        public DatabaseSql (string server, int port, string database, string uid, string password) {
            _server = server;
            _port = port;
            _database = database;
            _uid = uid;
            _password = password;
        }

        public bool InitConnection (IStatusReporter reporter) {
            try {
                _factory = DbProviderFactories.GetFactory ("MySql.Data.MySqlClient");

                DbConnectionStringBuilder builder = _factory.CreateConnectionStringBuilder ();
                builder.Add ("server", _server);
                builder.Add ("port", _port);
                builder.Add ("database", _database);
                builder.Add ("uid", _uid);
                builder.Add ("password", _password);

                _connection = _factory.CreateConnection ();
                _connection.ConnectionString = builder.ConnectionString;

            } catch (Exception ex) {
                reporter.SetStatus ("Keine Verbindung zur Datenbank: " + ex.Message);
                _connection = null;
                return false;
            }

            try {
                _connection.Open ();
            } catch (Exception ex) {
                reporter.SetStatus ("Verbindung fehlgeschlagen: " + ex.Message);
                _connection = null;
                return false;
            }
            return true;
        }

        public DbConnection GetOrCreateConnection () {
            return _connection;
        }
    }
}
