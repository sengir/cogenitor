﻿using System;
using System.Data.Common;

namespace Cognitor.Db {
    public interface IDatabase {
        bool InitConnection (IStatusReporter reporter);
        DbConnection GetOrCreateConnection ();
    }
}
