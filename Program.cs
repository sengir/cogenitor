﻿using System;
using Cogenitor.Gui;
using Cognitor.Utils;
using Gtk;

namespace Cognitor {
    class MainClass {
        public static void Main (string [] args) {

            InitDirectories ();

            Application.Init ();
            LoginWindow win = new LoginWindow ();
            win.Show ();
            win.KeepAbove = true;
            Application.Run ();
        }

        public static void InitDirectories () {
            Globals.Folders = new FolderManager (Globals.PATH_APPDATA, Globals.InstancePath);
            Globals.Folders.DefineFolder (Globals.PATH_LOG, Environment.SpecialFolder.Personal, Globals.PATH_LOG, true, true);
            Globals.Folders.DefineFolder (Globals.PATH_SETTINGS, Environment.SpecialFolder.Personal, Globals.PATH_SETTINGS, true, true);
            Globals.Folders.DefineFolder (Globals.PATH_CACHE, Environment.SpecialFolder.ApplicationData, Globals.PATH_CACHE, true, true);
        }
    }


}
