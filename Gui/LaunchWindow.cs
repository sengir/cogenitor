﻿using System;
using System.Threading;
using Cognitor;
using Cognitor.Data;
using Cognitor.Db;
using Gtk;

namespace Cogenitor.Gui {
    public sealed class LaunchWindow : BaseWindow, IStatusReporter {

        ProgressBar _barStatus;
        Label _labelStatus;

        IDatabase _db;

        public LaunchWindow (IDatabase db)
            : base (512, 256) {
            _db = db;

            Build ();
            Thread lthread = new Thread (LoadApplication);
            lthread.Start ();
        }

        #region Initial Build
        protected override void Build () {
            Name = "LaunchWindow";
            Title = global::Mono.Unix.Catalog.GetString ("Cogenitor - Startvorgang");
            Resizable = false;

            VBox vbox0 = new VBox ();
            Alignment align0 = new Alignment (0f, 0f, 1f, 1f);
            align0.Add (vbox0);

            Alignment align1 = new Alignment (0f, 0f, 1f, 1f);
            align1.WidthRequest = 300;
            vbox0.Add (align1);

            _barStatus = new ProgressBar ();
            align1.Add (_barStatus);

            Alignment align2 = new Alignment (0f, 0f, 1f, 1f);
            align2.WidthRequest = 300;
            align2.TopPadding = 6;
            align2.BottomPadding = 6;
            vbox0.Add (align2);

            _labelStatus = new Label ("Starte...");
            align2.Add (_labelStatus);

            Alignment align3 = new Alignment (0f, 0f, 1f, 1f);
            align3.WidthRequest = 300;
            vbox0.Add (align3);

            Button btn0 = new Button ("gtk_cancel");
            align3.Add (btn0);

            Add (align0);
            if ((Child != null)) {
                Child.ShowAll ();
            }

            Show ();
            DeleteEvent += new global::Gtk.DeleteEventHandler (this.OnDeleteEvent);
        }
        #endregion

        #region Interactions
        private void OnDeleteEvent (object sender, DeleteEventArgs a) {
            Application.Quit ();
            a.RetVal = true;
        }
        #endregion

        private void LoadApplication () {
            SetProgress (0.25f);
            PluginManager.Init (this);
            SetProgress (0.5f);
            SetStatus ("Initialisiere Datenbank...");
            SetProgress (0.75f);
            using (LawContext ctx = new LawContext (_db.GetOrCreateConnection ())) {
                Party party = new Party () { FirstName = "Some", LastName = "Guy" };
                ctx.Parties.Add (party);

                LawFile file = new LawFile ();
                Involvement involved = new Involvement () {
                    Party = party,
                    Kind = InvolvementKind.Client
                };
                file.Involvements.Add (involved);
                ctx.Files.Add (file);

                ctx.SaveChanges ();
            }
            Application.Invoke (delegate {
                SwitchToMain ();
            });
        }

        public void SetStatus (string status) {
            Application.Invoke (delegate {
                _labelStatus.Text = status;
            });

            Console.Out.WriteLine (status);
        }

        public void SetProgress (float progress) {
            Application.Invoke (delegate {
                _barStatus.Fraction = progress;
            });
        }

        private void SwitchToMain () {
            CoreWindow main = new CoreWindow (_db);
            main.Show ();
            Destroy ();
        }
    }
}
