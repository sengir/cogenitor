﻿using System;
using Cognitor;
using Cognitor.Db;
using Gtk;

namespace Cogenitor.Gui {

    public sealed class LoginWindow : BaseWindow, IStatusReporter {

        VBox vbox2;
        Frame frame2;
        Alignment GtkAlignment1;
        HBox hbox6;
        HSeparator hseparator2;
        Frame frame1;
        Alignment GtkAlignment;
        HSeparator hseparator3;
        HBox hbox7;
        Table table1;

        Entry entry5;
        Entry entry6;

        Label GtkLabel3;
        Label label6;
        Label label7;
        Label GtkLabel2;

        ComboBox combobox4;

        Button button3;
        Button btnStart;
        Button btnClose;

        public LoginWindow ()
            : base (512, 256) {

            Build ();
        }

        #region Initial Build
        protected override void Build () {

            // Widget MainWindow
            Name = "LoginWindow";
            Title = global::Mono.Unix.Catalog.GetString ("Cogenitor");
            Resizable = false;

            // Container child MainWindow.Gtk.Container+ContainerChild
            this.vbox2 = new global::Gtk.VBox ();
            this.vbox2.Name = "vbox2";
            this.vbox2.Spacing = 6;
            // Container child vbox2.Gtk.Box+BoxChild
            this.frame2 = new global::Gtk.Frame ();
            this.frame2.Name = "frame2";
            this.frame2.ShadowType = ((global::Gtk.ShadowType)(0));
            // Container child frame2.Gtk.Container+ContainerChild
            this.GtkAlignment1 = new global::Gtk.Alignment (0F, 0F, 1F, 1F);
            this.GtkAlignment1.Name = "GtkAlignment1";
            this.GtkAlignment1.LeftPadding = ((uint)(12));
            // Container child GtkAlignment1.Gtk.Container+ContainerChild
            this.hbox6 = new global::Gtk.HBox ();
            this.hbox6.Name = "hbox6";
            this.hbox6.Spacing = 6;
            // Container child hbox6.Gtk.Box+BoxChild
            this.combobox4 = global::Gtk.ComboBox.NewText ();
            this.combobox4.WidthRequest = 300;
            this.combobox4.Name = "combobox4";
            this.hbox6.Add (this.combobox4);
            global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.hbox6 [this.combobox4]));
            w1.Position = 0;
            w1.Expand = false;
            w1.Fill = false;
            // Container child hbox6.Gtk.Box+BoxChild
            this.button3 = new global::Gtk.Button ();
            this.button3.CanFocus = true;
            this.button3.Name = "button3";
            this.button3.UseUnderline = true;
            this.button3.Label = global::Mono.Unix.Catalog.GetString ("Hinzufügen");
            this.hbox6.Add (this.button3);
            global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox6 [this.button3]));
            w2.Position = 1;
            w2.Expand = false;
            w2.Fill = false;
            this.GtkAlignment1.Add (this.hbox6);
            this.frame2.Add (this.GtkAlignment1);
            this.GtkLabel3 = new global::Gtk.Label ();
            this.GtkLabel3.Name = "GtkLabel3";
            this.GtkLabel3.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Datenbank:</b>");
            this.GtkLabel3.UseMarkup = true;
            this.frame2.LabelWidget = this.GtkLabel3;
            this.vbox2.Add (this.frame2);
            global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.frame2]));
            w5.Position = 0;
            w5.Expand = false;
            w5.Fill = false;
            // Container child vbox2.Gtk.Box+BoxChild
            this.hseparator2 = new global::Gtk.HSeparator ();
            this.hseparator2.Name = "hseparator2";
            this.vbox2.Add (this.hseparator2);
            global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.hseparator2]));
            w6.Position = 1;
            w6.Expand = false;
            w6.Fill = false;
            // Container child vbox2.Gtk.Box+BoxChild
            this.frame1 = new global::Gtk.Frame ();
            this.frame1.Name = "frame1";
            this.frame1.ShadowType = ((global::Gtk.ShadowType)(0));
            // Container child frame1.Gtk.Container+ContainerChild
            this.GtkAlignment = new global::Gtk.Alignment (0F, 0F, 1F, 1F);
            this.GtkAlignment.Name = "GtkAlignment";
            this.GtkAlignment.LeftPadding = ((uint)(12));
            // Container child GtkAlignment.Gtk.Container+ContainerChild
            this.table1 = new global::Gtk.Table (((uint)(2)), ((uint)(2)), false);
            this.table1.Name = "table1";
            this.table1.RowSpacing = ((uint)(6));
            this.table1.ColumnSpacing = ((uint)(6));
            // Container child table1.Gtk.Table+TableChild
            this.entry5 = new global::Gtk.Entry ();
            this.entry5.CanFocus = true;
            this.entry5.Name = "entry5";
            this.entry5.IsEditable = true;
            this.entry5.InvisibleChar = '●';
            this.table1.Add (this.entry5);
            global::Gtk.Table.TableChild w7 = ((global::Gtk.Table.TableChild)(this.table1 [this.entry5]));
            w7.LeftAttach = ((uint)(1));
            w7.RightAttach = ((uint)(2));
            w7.YOptions = ((global::Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.entry6 = new global::Gtk.Entry ();
            this.entry6.CanFocus = true;
            this.entry6.Name = "entry6";
            this.entry6.IsEditable = true;
            this.entry6.InvisibleChar = '●';
            this.table1.Add (this.entry6);
            global::Gtk.Table.TableChild w8 = ((global::Gtk.Table.TableChild)(this.table1 [this.entry6]));
            w8.TopAttach = ((uint)(1));
            w8.BottomAttach = ((uint)(2));
            w8.LeftAttach = ((uint)(1));
            w8.RightAttach = ((uint)(2));
            w8.YOptions = ((global::Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label6 = new global::Gtk.Label ();
            this.label6.Name = "label6";
            this.label6.LabelProp = global::Mono.Unix.Catalog.GetString ("Nutzer:");
            this.table1.Add (this.label6);
            global::Gtk.Table.TableChild w9 = ((global::Gtk.Table.TableChild)(this.table1 [this.label6]));
            w9.XOptions = ((global::Gtk.AttachOptions)(4));
            w9.YOptions = ((global::Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label7 = new global::Gtk.Label ();
            this.label7.Name = "label7";
            this.label7.LabelProp = global::Mono.Unix.Catalog.GetString ("Passwort:");
            this.table1.Add (this.label7);
            global::Gtk.Table.TableChild w10 = ((global::Gtk.Table.TableChild)(this.table1 [this.label7]));
            w10.TopAttach = ((uint)(1));
            w10.BottomAttach = ((uint)(2));
            w10.XOptions = ((global::Gtk.AttachOptions)(4));
            w10.YOptions = ((global::Gtk.AttachOptions)(4));
            this.GtkAlignment.Add (this.table1);
            this.frame1.Add (this.GtkAlignment);
            this.GtkLabel2 = new global::Gtk.Label ();
            this.GtkLabel2.Name = "GtkLabel2";
            this.GtkLabel2.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Anmeldung:</b>");
            this.GtkLabel2.UseMarkup = true;
            this.frame1.LabelWidget = this.GtkLabel2;
            this.vbox2.Add (this.frame1);
            global::Gtk.Box.BoxChild w13 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.frame1]));
            w13.Position = 2;
            w13.Expand = false;
            w13.Fill = false;
            // Container child vbox2.Gtk.Box+BoxChild
            this.hseparator3 = new global::Gtk.HSeparator ();
            this.hseparator3.Name = "hseparator3";
            this.vbox2.Add (this.hseparator3);
            global::Gtk.Box.BoxChild w14 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.hseparator3]));
            w14.Position = 3;
            w14.Expand = false;
            w14.Fill = false;
            // Container child vbox2.Gtk.Box+BoxChild
            this.hbox7 = new global::Gtk.HBox ();
            this.hbox7.Name = "hbox7";
            this.hbox7.Spacing = 6;
            // Container child hbox7.Gtk.Box+BoxChild
            this.btnStart = new global::Gtk.Button ();
            this.btnStart.CanFocus = true;
            this.btnStart.Name = "btnStart";
            this.btnStart.UseUnderline = true;
            this.btnStart.Label = global::Mono.Unix.Catalog.GetString ("Anmelden");
            this.hbox7.Add (this.btnStart);
            global::Gtk.Box.BoxChild w15 = ((global::Gtk.Box.BoxChild)(this.hbox7 [this.btnStart]));
            w15.Position = 0;
            w15.Expand = false;
            w15.Fill = false;
            // Container child hbox7.Gtk.Box+BoxChild
            this.btnClose = new global::Gtk.Button ();
            this.btnClose.CanFocus = true;
            this.btnClose.Name = "btnClose";
            this.btnClose.UseUnderline = true;
            this.btnClose.Label = global::Mono.Unix.Catalog.GetString ("Schließen");
            this.hbox7.Add (this.btnClose);
            global::Gtk.Box.BoxChild w16 = ((global::Gtk.Box.BoxChild)(this.hbox7 [this.btnClose]));
            w16.Position = 1;
            w16.Expand = false;
            w16.Fill = false;
            this.vbox2.Add (this.hbox7);
            global::Gtk.Box.BoxChild w17 = ((global::Gtk.Box.BoxChild)(this.vbox2 [this.hbox7]));
            w17.Position = 4;
            w17.Expand = false;
            w17.Fill = false;
            this.Add (this.vbox2);
            if ((this.Child != null)) {
                this.Child.ShowAll ();
            }

            Show ();

            DeleteEvent += new global::Gtk.DeleteEventHandler (this.OnDeleteEvent);
            btnStart.Clicked += new global::System.EventHandler (this.OnBtnStartClicked);
            btnClose.Clicked += new global::System.EventHandler (this.OnBtnCloseClicked);

        }
        #endregion

        #region Interactions
        private void OnDeleteEvent (object sender, DeleteEventArgs a) {
            Application.Quit ();
            a.RetVal = true;
        }

        private void OnBtnCloseClicked (object sender, EventArgs e) {
            Application.Quit ();
        }

        private void OnBtnStartClicked (object sender, EventArgs e) {
            IDatabase db = new DatabaseSql ("localhost", "cognitor", "root", "password");
            db.InitConnection (this);

            LaunchWindow main = new LaunchWindow (db);
            main.Show ();

            Destroy ();
        }

        public void SetStatus (string status) {
            /*
            Application.Invoke (delegate {
                _labelStatus.Text = status;
            });
            */

            Console.Out.WriteLine (status);

        }
        #endregion

    }
}
