﻿using System;
using Gtk;

namespace Cogenitor.Gui {

    public abstract class BaseWindow : Gtk.Window {

        protected UIManager UiManager {
            get;
            set;
        }


        public BaseWindow (int width, int height)
        : base (Gtk.WindowType.Toplevel) {

            InitWindow (width, height);
        }

        protected void InitWindow (int width, int height) {
            UiManager = new UIManager ();

            WindowPosition = ((global::Gtk.WindowPosition)(1));
            BorderWidth = ((uint)(12));
            DefaultWidth = width;
            DefaultHeight = height;
        }

        protected abstract void Build ();
    }
}
