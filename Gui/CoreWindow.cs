﻿using System;
using Cognitor.Db;
using Gtk;

namespace Cogenitor.Gui {

    public sealed class CoreWindow : BaseWindow {

        Gtk.Action DateiAction;
        Gtk.Action BeendenAction;

        Gtk.Action addAction;
        Gtk.Action boldAction;
        Gtk.Action preferencesAction;
        Gtk.Action quitAction;
        Gtk.Action orientationPortraitAction;
        Gtk.Action mediaRecordAction;

        VBox vbox6;
        HBox hbox1;

        Label GtkLabel2;
        Label GtkLabel3;

        TreeView treeview1;

        Toolbar toolbar1;
        Statusbar statusbar1;

        Frame frame1;
        Alignment GtkAlignment;
        ScrolledWindow GtkScrolledWindow;
        Frame frame2;
        Alignment GtkAlignment1;

        IDatabase _db;

        public CoreWindow (IDatabase db)
            : base (1027, 768) {
            _db = db;

            Build ();
        }

        #region Initial Build
        protected override void Build () {
            Name = "Cognitor.MainMenu";
            Title = global::Mono.Unix.Catalog.GetString ("Cogenitor - Hauptmenü");

            ActionGroup w1 = new Gtk.ActionGroup ("Default");
            this.DateiAction = new Gtk.Action ("DateiAction", global::Mono.Unix.Catalog.GetString ("Datei"), null, null);
            this.DateiAction.ShortLabel = Mono.Unix.Catalog.GetString ("Datei");
            w1.Add (this.DateiAction, null);
            this.BeendenAction = new Gtk.Action ("BeendenAction", global::Mono.Unix.Catalog.GetString ("Beenden"), null, null);
            this.BeendenAction.ShortLabel = global::Mono.Unix.Catalog.GetString ("Beenden");
            w1.Add (this.BeendenAction, null);
            this.addAction = new global::Gtk.Action ("addAction", global::Mono.Unix.Catalog.GetString ("Neue Akte"), null, "gtk-add");
            this.addAction.ShortLabel = global::Mono.Unix.Catalog.GetString ("Neue Akte");
            w1.Add (this.addAction, null);
            this.boldAction = new global::Gtk.Action ("boldAction", global::Mono.Unix.Catalog.GetString ("Textverarbeitung"), null, "gtk-bold");
            this.boldAction.ShortLabel = global::Mono.Unix.Catalog.GetString ("Textverarbeitung");
            w1.Add (this.boldAction, null);
            this.preferencesAction = new global::Gtk.Action ("preferencesAction", null, null, "gtk-preferences");
            w1.Add (this.preferencesAction, null);
            this.quitAction = new global::Gtk.Action ("quitAction", null, null, "gtk-quit");
            w1.Add (this.quitAction, null);
            this.orientationPortraitAction = new global::Gtk.Action ("orientationPortraitAction", global::Mono.Unix.Catalog.GetString ("Adressverwaltung"), null, "gtk-orientation-portrait");
            this.orientationPortraitAction.ShortLabel = global::Mono.Unix.Catalog.GetString ("Adressverwaltung");
            w1.Add (this.orientationPortraitAction, null);
            this.mediaRecordAction = new global::Gtk.Action ("mediaRecordAction", global::Mono.Unix.Catalog.GetString ("Telefonnotiz"), null, "gtk-media-record");
            this.mediaRecordAction.ShortLabel = global::Mono.Unix.Catalog.GetString ("Telefonnotiz");
            w1.Add (this.mediaRecordAction, null);
            UiManager.InsertActionGroup (w1, 0);

            this.AddAccelGroup (UiManager.AccelGroup);

            // Container child Cognitor.MainMenu.Gtk.Container+ContainerChild
            this.vbox6 = new global::Gtk.VBox ();
            this.vbox6.Name = "vbox6";
            this.vbox6.Spacing = 6;
            // Container child vbox6.Gtk.Box+BoxChild
            UiManager.AddUiFromString ("<ui><toolbar name='toolbar1'><toolitem name='addAction' action='addAction'/><toolitem name='orientationPortraitAction' action='orientationPortraitAction'/><toolitem name='boldAction' action='boldAction'/><toolitem name='mediaRecordAction' action='mediaRecordAction'/><toolitem name='preferencesAction' action='preferencesAction'/><toolitem name='quitAction' action='quitAction'/></toolbar></ui>");
            this.toolbar1 = ((global::Gtk.Toolbar)(UiManager.GetWidget ("/toolbar1")));
            this.toolbar1.Name = "toolbar1";
            this.toolbar1.ShowArrow = false;
            this.vbox6.Add (this.toolbar1);
            Box.BoxChild w2 = ((Box.BoxChild)(this.vbox6 [this.toolbar1]));
            w2.Position = 0;
            w2.Expand = false;
            w2.Fill = false;
            // Container child vbox6.Gtk.Box+BoxChild
            this.hbox1 = new global::Gtk.HBox ();
            this.hbox1.Name = "hbox1";
            this.hbox1.Spacing = 6;
            // Container child hbox1.Gtk.Box+BoxChild
            this.frame1 = new global::Gtk.Frame ();
            this.frame1.WidthRequest = 256;
            this.frame1.Name = "frame1";
            this.frame1.ShadowType = ((global::Gtk.ShadowType)(0));
            // Container child frame1.Gtk.Container+ContainerChild
            this.GtkAlignment = new global::Gtk.Alignment (0F, 0F, 1F, 1F);
            this.GtkAlignment.Name = "GtkAlignment";
            this.GtkAlignment.LeftPadding = ((uint)(12));
            // Container child GtkAlignment.Gtk.Container+ContainerChild
            this.GtkScrolledWindow = new global::Gtk.ScrolledWindow ();
            this.GtkScrolledWindow.Name = "GtkScrolledWindow";
            this.GtkScrolledWindow.ShadowType = ((global::Gtk.ShadowType)(1));
            // Container child GtkScrolledWindow.Gtk.Container+ContainerChild
            this.treeview1 = new global::Gtk.TreeView ();
            this.treeview1.CanFocus = true;
            this.treeview1.Name = "treeview1";
            this.GtkScrolledWindow.Add (this.treeview1);
            this.GtkAlignment.Add (this.GtkScrolledWindow);
            this.frame1.Add (this.GtkAlignment);
            this.GtkLabel2 = new global::Gtk.Label ();
            this.GtkLabel2.Name = "GtkLabel2";
            this.GtkLabel2.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Offene Akten:</b>");
            this.GtkLabel2.UseMarkup = true;
            this.frame1.LabelWidget = this.GtkLabel2;
            this.hbox1.Add (this.frame1);
            global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.frame1]));
            w6.Position = 0;
            w6.Expand = false;
            w6.Fill = false;
            // Container child hbox1.Gtk.Box+BoxChild
            this.frame2 = new global::Gtk.Frame ();
            this.frame2.Name = "frame2";
            this.frame2.ShadowType = ((global::Gtk.ShadowType)(0));
            // Container child frame2.Gtk.Container+ContainerChild
            this.GtkAlignment1 = new global::Gtk.Alignment (0F, 0F, 1F, 1F);
            this.GtkAlignment1.Name = "GtkAlignment1";
            this.GtkAlignment1.LeftPadding = ((uint)(12));
            this.frame2.Add (this.GtkAlignment1);
            this.GtkLabel3 = new global::Gtk.Label ();
            this.GtkLabel3.Name = "GtkLabel3";
            this.GtkLabel3.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Akte</b>");
            this.GtkLabel3.UseMarkup = true;
            this.frame2.LabelWidget = this.GtkLabel3;
            this.hbox1.Add (this.frame2);
            global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.hbox1 [this.frame2]));
            w8.Position = 1;
            this.vbox6.Add (this.hbox1);
            global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.hbox1]));
            w9.Position = 1;
            // Container child vbox6.Gtk.Box+BoxChild
            this.statusbar1 = new global::Gtk.Statusbar ();
            this.statusbar1.Name = "statusbar1";
            this.statusbar1.Spacing = 6;
            this.vbox6.Add (this.statusbar1);
            global::Gtk.Box.BoxChild w10 = ((global::Gtk.Box.BoxChild)(this.vbox6 [this.statusbar1]));
            w10.Position = 2;
            w10.Expand = false;
            w10.Fill = false;
            Add (this.vbox6);
            if ((Child != null)) {
                Child.ShowAll ();
            }
            Show ();
            DeleteEvent += new DeleteEventHandler (OnDeleteEvent);

        }
        #endregion

        #region Interactions
        void OnDeleteEvent (object o, DeleteEventArgs args) {
            Application.Quit ();
            args.RetVal = true;
        }
        #endregion

    }

}
