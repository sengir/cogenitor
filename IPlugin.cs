﻿using System;
namespace Cognitor {
    public interface IPlugin {

        Version Version {
            get;
        }

        int Weight {
            get;
        }
    }
}
