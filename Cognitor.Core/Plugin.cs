﻿using System;
namespace Cognitor.Core {
    public sealed class Plugin : IPlugin {

        public Version Version {
            get {
                return new Version (1, 0, 0, 0);
            }
        }

        public int Weight {
            get {
                return 0;
            }
        }
    }
}
