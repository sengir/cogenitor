/*
* Copyright (c) 2014 SirSengir
*/

using System.Collections.Generic;
using System;

namespace Cognitor.Json {

    public sealed class JsonObject : IReadOnlyDictionary<string, JsonNode> {

        #region Properties

        public JsonNode this [string index] {
            get {
                return _dictionary [index];
            }
            set {
                _dictionary [index] = value;
            }
        }

        public IEnumerable<string> Keys {
            get {
                return _dictionary.Keys;
            }
        }

        public IEnumerable<JsonNode> Values {
            get {
                return _dictionary.Values;
            }
        }

        public int Count {
            get {
                return _dictionary.Count;
            }
        }

        #endregion

        readonly IDictionary<string, JsonNode> _dictionary;

        public JsonObject (params Tuple<string, JsonNode> [] nodes) {
            _dictionary = new Dictionary<string, JsonNode> ();
            foreach (Tuple<string, JsonNode> node in nodes) {
                _dictionary [node.Item1] = node.Item2;
            }
        }

        public JsonObject (IDictionary<string, JsonNode> dictionary) {
            _dictionary = dictionary;
        }

        public JsonObject () {
            _dictionary = new Dictionary<string, JsonNode> ();
        }

        public bool ContainsKey (string key) {
            return _dictionary.ContainsKey (key);
        }

        public void Set (string key, string value) {
            _dictionary [key] = new JsonNode<string> (JsonValueType.String, value);
        }

        public void Set (string key, double value) {
            _dictionary [key] = new JsonNode<double> (JsonValueType.Number, value);
        }

        public void Set (string key, JsonObject value) {
            _dictionary [key] = new JsonNode<JsonObject> (JsonValueType.Object, value);
        }

        public void Set (string key, JsonArray value) {
            _dictionary [key] = new JsonNode<JsonArray> (JsonValueType.Array, value);
        }

        public void Set (string key, object value) {
            if (value is string) {
                Set (key, (string)value);
                return;
            }
            if (value is JsonObject) {
                Set (key, (JsonObject)value);
                return;
            }
            if (value is JsonArray) {
                Set (key, (JsonArray)value);
                return;
            }

            Set (key, Convert.ToDouble (value));
        }

        public bool TryGetValue (string key, out JsonNode value) {
            return _dictionary.TryGetValue (key, out value);
        }

        public IEnumerator<KeyValuePair<string, JsonNode>> GetEnumerator () {
            return _dictionary.GetEnumerator ();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator () {
            return _dictionary.GetEnumerator ();
        }
    }
}

