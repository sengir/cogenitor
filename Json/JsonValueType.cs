namespace Cognitor.Json {

    public enum JsonValueType {
        None,
        Null,
        Boolean,
        String,
        Number,
        Object,
        Array
    }
}

