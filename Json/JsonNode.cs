using System.Collections.Generic;

namespace Cognitor.Json {

    public abstract class JsonNode {

        public JsonValueType Type {
            get;
            private set;
        }

        object _value;

        public JsonNode (JsonValueType type, object value) {
            Type = type;
            _value = value;
        }

        public T GetValue<T> () {
            return (T)_value;
        }

        /// <summary>
        /// Convenience function which assumes this node to be a JsonArray and returns the array as an enumerable of the given type.
        /// </summary>
        /// <returns>The enumerable.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public IEnumerable<T> AsEnumerable<T> () {
            return ((JsonArray)_value).GetEnumerable<T> ();
        }
    }

    public sealed class JsonNode<T> : JsonNode {

        public T Value {
            get;
            private set;
        }

        public JsonNode (JsonValueType type, T value)
            : base (type, value) {
            Value = value;
        }

    }
}

