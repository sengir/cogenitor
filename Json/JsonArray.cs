/*
* Copyright (c) 2014 SirSengir
*/

using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace Cognitor.Json {

    public sealed class JsonArray : IReadOnlyList<JsonNode> {
        #region Classes

        sealed class JsonArrayEnumerator<T> : IEnumerator<T> {
            int _current = -1;
            JsonArray _target;

            public JsonArrayEnumerator (JsonArray targetList) {
                _target = targetList;
            }

            object IEnumerator.Current {
                get { return Current; }
            }

            public T Current {
                get { return _target [_current].GetValue<T> (); }
            }

            public bool MoveNext () {
                if (_current < 0) {
                    _current = 0;
                } else {
                    _current++;
                }
                return _current < _target.Count;
            }

            public void Reset () {
                _current = -1;
            }

            public void Dispose () {

            }
        }

        #endregion

        #region Properties

        public JsonNode this [int index] {
            get {
                return _list [index];
            }
        }

        public int Count {
            get {
                return _list.Count;
            }
        }

        #endregion

        IList<JsonNode> _list;

        public JsonArray (IList<JsonNode> list) {
            _list = list;
        }

        public JsonArray () {
            _list = new List<JsonNode> ();
        }

        public void Append (JsonNode node) {
            _list.Add (node);
        }

        public void Append (JsonObject json) {
            JsonNode<JsonObject> node = new JsonNode<JsonObject> (JsonValueType.Object, json);
            _list.Add (node);
        }

        public IEnumerator<JsonNode> GetEnumerator () {
            return _list.GetEnumerator ();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator () {
            return _list.GetEnumerator ();
        }

        public IEnumerable<T> GetEnumerable<T> () {
            return _list.Select (p => p.GetValue<T> ());
        }
    }
}

