﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Cognitor.Utils {

    public sealed class FolderManager {

        public ApplicationFolder this [string folder] {
            get {
                return _folders [folder];
            }
        }

        public FileInfo this [string folder, string file] {
            get {
                return new FileInfo (Path.Combine (_folders [folder].Location.FullName, file));
            }
        }

        public string InstancePath {
            get;
            private set;
        }

        Dictionary<string, ApplicationFolder> _folders = new Dictionary<string, ApplicationFolder> ();
        string _appRoot;

        public FolderManager (string appRoot, string instancePath) {
            _appRoot = appRoot;
            InstancePath = instancePath;
        }

        public void DefineFolder (string ident, Environment.SpecialFolder system, string path, bool versioned, bool autocreate) {
            _folders [ident] = new ApplicationFolder (system, _appRoot, InstancePath, path, versioned, autocreate);
        }

        public string GetFilePath (string folder, string file) {
            return Path.Combine (_folders [folder].Location.FullName, file);
        }

        public void SetInstancePath (string instancePath) {
            InstancePath = instancePath;
            foreach (var entry in _folders) {
                entry.Value.SetInstancePath (InstancePath);
            }
        }

        public void SetPaths (string appRoot, string instancePath) {
            _appRoot = appRoot;
            InstancePath = instancePath;
            foreach (var entry in _folders) {
                entry.Value.SetPaths (_appRoot, InstancePath);
            }
        }
    }
}

