﻿using System;
using System.IO;

namespace Cognitor.Utils {

    public sealed class ApplicationFolder {
        #region Properties

        public DirectoryInfo Location {
            get;
            private set;
        }

        #endregion

        Environment.SpecialFolder _folder;
        string _root;
        string _instance;
        string _path;
        bool _versioned;
        bool _autocreate;

        public ApplicationFolder (Environment.SpecialFolder folder, string root, string instance, string path, bool versioned, bool autocreate) {
            _folder = folder;
            _root = root;
            _instance = instance;
            _path = path;
            _versioned = versioned;
            _autocreate = autocreate;

            SetLocation ();
        }

        private void SetLocation () {
            if (_versioned) {
                Location = new DirectoryInfo (Path.Combine (Environment.GetFolderPath (_folder), _root, _instance, _path));
            } else {
                Location = new DirectoryInfo (Path.Combine (Environment.GetFolderPath (_folder), _root, _path));
            }
            CreateIfNeeded ();
        }

        private void CreateIfNeeded () {
            if (_autocreate) {
                if (!Location.Exists) {
                    Location.Create ();
                }
            }
        }

        public string GetFileWithin (string file) {
            return Path.Combine (Location.FullName, file);
        }

        public string ExtractSubPath (string filePath) {
            return filePath.Replace (Location.FullName, "");
        }

        internal void SetInstancePath (string instancePath) {
            _instance = instancePath;
            SetLocation ();
        }

        internal void SetPaths (string appRoot, string instancePath) {
            _root = appRoot;
            _instance = instancePath;
            SetLocation ();
        }
    }
}
