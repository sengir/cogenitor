using System;
using System.Text;
using System.Linq;

namespace Cognitor.Utils {

    public static class StringUtils {
        public static string ToFilePath (string resource) {
            int last = resource.LastIndexOf ('.');
            string query = resource.Substring (0, last);
            string suffix = resource.Substring (last);
            return query.Replace (".", "/") + suffix;
        }

        public static string Capitalize (string input) {
            if (string.IsNullOrWhiteSpace (input))
                return input;

            char [] arr = input.ToCharArray ();
            arr [0] = Char.ToUpperInvariant (arr [0]);
            return new String (arr);
        }

        public static string Uncapitalize (string input) {
            if (string.IsNullOrWhiteSpace (input))
                return input;

            char [] arr = input.ToCharArray ();
            arr [0] = Char.ToLowerInvariant (arr [0]);
            return new String (arr);
        }

        public static string BuildListing (string delim, params string [] parts) {
            if (parts.Length <= 0)
                return string.Empty;

            StringBuilder builder = new StringBuilder ();
            for (int i = 0; i < parts.Length; i++) {
                if (builder.Length > 0)
                    builder.Append (delim);
                builder.Append (parts [i]);
            }

            return builder.ToString ();
        }

        const string MOD_PERCENT = "{0:§\\#\\0\\0ff\\0\\0§+0%;§\\#fa8\\072§-0%;§\\#\\0\\0ee\\0\\0§0%}";

        /// <summary>
        /// Determines whether the given string consists only of numbers and letters.
        /// </summary>
        /// <returns><c>true</c>, if the string consisted only of numbers and letters, <c>false</c> otherwise.</returns>
        /// <param name="input">Input.</param>
        public static bool ConsistsOfOnlyNumbersAndLetters (string input) {
            return input.All (Char.IsLetterOrDigit);
        }
    }
}

