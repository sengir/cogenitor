﻿using System;
using System.Reflection;

namespace Cognitor.Utils {
    public static class ReflectionUtils {

        /// <summary>
        /// Look through the list of loaded assemblies and try to find the given one, otherwise try to load it.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Assembly SearchOrLoadAssembly (string name) {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies ()) {
                AssemblyName assemName = assembly.GetName ();
                if (assemName.Name.Equals (name)) {
                    return assembly;
                }
            }
            try {
                return Assembly.Load (name);
            } catch (Exception ex) {
                Console.Out.WriteLine ("Failed to load assembly: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Loads the assembly from the given file.
        /// </summary>
        /// <returns>The assembly from file.</returns>
        /// <param name="file">The file to load from.</param>
        public static Assembly LoadAssemblyFromFile (string file) {
            try {
                return Assembly.LoadFrom (file);
            } catch (Exception ex) {
                Console.Out.WriteLine ("Failed to load assembly: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Look through the list of loaded assemblies and try to find the given one.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Assembly SearchAssembly (string name) {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies ()) {
                AssemblyName assemName = assembly.GetName ();
                if (assemName.Name.Equals (name)) {
                    return assembly;
                }
            }
            return null;
        }


    }
}
