﻿using System;
using System.Collections.Generic;
using System.IO;
using Cognitor.Json;

namespace Cognitor.Utils {
    public sealed class SettingsManager {
        string _filepath;
        Dictionary<string, Dictionary<string, object>> _settings = new Dictionary<string, Dictionary<string, object>> ();

        public SettingsManager (string filepath) {
            //_filepath = GameAccess.Folders.GetFilePath (Constants.PATH_SETTINGS, file);
            _filepath = filepath;
            CreateDefaultIfNeeded (_filepath);
            if (ValidateFile (_filepath)) {
                ParseSettings (_filepath);
            }
        }

        void CreateDefaultIfNeeded (string filepath) {
            if (ValidateFile (filepath)) {
                return;
            }

            string defaultsettings = @"{
    ""profile"": [
        {
            ""key"": ""login"",
            ""value"": ""ThePlayer""
        }
    ],
    ""video"": [
        {
            ""key"": ""mode"",
            ""value"": ""window""
        },
        {
            ""key"": ""resolution"",
            ""value"": ""1600x900""
        },
        {
            ""key"": ""shadows"",
            ""value"": true
        }
    ]
    ""sound"": [
        {
            ""key"": ""effects"",
            ""value"": true
        },
        {
            ""key"": ""music"",
            ""value"": true
        },
    ]
}";
            File.WriteAllText (filepath, defaultsettings);
        }

        bool ValidateFile (string filepath) {
            return File.Exists (filepath);
        }

        void ParseSettings (string filepath) {
            string json = File.ReadAllText (filepath);
            JsonObject result = JsonParser.JsonDecode (json).GetValue<JsonObject> ();
            foreach (KeyValuePair<string, JsonNode> entry in result) {
                string section = entry.Key;
                if (!_settings.ContainsKey (section)) {
                    _settings [section] = new Dictionary<string, object> ();
                }

                foreach (var setting in entry.Value.AsEnumerable<JsonObject> ()) {
                    _settings [section] [setting ["key"].GetValue<string> ()] = setting ["value"].GetValue<object> ();
                }
            }
        }

        /// <summary>
        /// Determines whether the given key is present in the given section.
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool HasKey (string section, string key) {
            if (!_settings.ContainsKey (section))
                return false;
            return _settings [section].ContainsKey (key);
        }

        public T Get<T> (string section, string key) {
            if (!_settings.ContainsKey (section))
                return default (T);
            if (!_settings [section].ContainsKey (key))
                return default (T);
            return (T)_settings [section] [key];
        }

        public void Set (string section, string key, object value) {
            if (!_settings.ContainsKey (section))
                _settings [section] = new Dictionary<string, object> ();

            _settings [section] [key] = value;
        }

        public void Flush () {
            JsonObject sections = new JsonObject ();
            foreach (KeyValuePair<string, Dictionary<string, object>> section in _settings) {
                JsonArray settings = new JsonArray ();
                foreach (KeyValuePair<string, object> entry in section.Value) {
                    JsonObject setting = new JsonObject ();
                    setting.Set ("key", entry.Key);
                    setting.Set ("value", entry.Value);
                    settings.Append (setting);
                }
                sections.Set (section.Key, settings);
            }

            string json = JsonParser.JsonEncode (new JsonNode<JsonObject> (JsonValueType.Object, sections));
            File.WriteAllText (_filepath, json);
        }
    }
}
